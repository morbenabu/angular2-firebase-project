import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {ChatRoomsService} from '../chat-rooms/chat-rooms.service';
import {NgForm} from '@angular/forms';
import {AF} from "./../providers/af";
import { FirebaseListObservable, AngularFire, FirebaseObjectObservable } from "angularfire2";
import {ChatRooms} from './chat-rooms';
import {FirebaseObjectFactoryOpts} from "angularfire2/interfaces";
import * as firebase from 'firebase';

@Component({
  selector: 'app-chat-rooms',
  templateUrl: './chat-rooms.component.html',
  styleUrls: ['./chat-rooms.component.css'],
/*styles: [`
./home-page.component.css
    .users li { cursor: default; }
    .users li:hover { background: #ecf0f1};
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }      
  `]*/
 

})
export class ChatRoomsComponent implements OnInit {

  user:ChatRooms;
  arruser=[];
  groupsObservable;
  public group:string;
  groups;
  users;
  currentUser;
  currentUser2;
  public name:string;
  saveGroupName;
  public myName;
  currentName;
  mail;
  public p=0;
  bool=-1;
  nameOfGroup="-1";
  myMail;
  ifClicked;
  ifClicked1;
 

 select(user,group){
   if(this.arruser[0]!=firebase.auth().currentUser.email )
   {
     this.arruser.push(firebase.auth().currentUser.email);
   }
		this.currentUser = user; 
    this.currentUser2 = user; 
    this.arruser.push(user.email);
    console.log(this.arruser);
   console.log(user);
 }

 cancel(user){
 var i=0;
 for(i = 0;i<this.arruser.length; i++)
 {
    if(user.email==this.arruser[i])
      break;
 }
   this.arruser.splice(i,1);
   console.log(this.arruser);
   console.log(this.arruser.length);
   

 }

  select2(group){
    		this.currentUser2 = group; 
    this.nameOfGroup=this.currentUser2.name;
    this.afService.nameOfGroup=this.nameOfGroup;
   console.log(this.currentUser2.name);
 }

  addGroup(){
    if(this.name==""||this.name==null){
    alert("please enter group name");
      return;
    }
      for(var i = 0 ; i<100;i++)
      {
        if(this.groups[i] != undefined)
        { 
        if(this.name == this.groups[i].name)
        {
            alert("This group name is not available");
            return;
        }
        }
      }
    
    this.afService.addGroup(this.name,this.arruser);
    this.saveGroupName=this.name;
    this.arruser=[];
    console.log(this.name);
    this.name = '';
    this.myMail=false;
    this.mail=true;

  }

  getMyMail(){  
    this.mail=true;
      this.myName =firebase.auth().currentUser.email;
      this.ifClicked1=true;
      this.ifClicked=false;
      console.log(this.myName);
      console.log(this.groups[0].userArr);
  }
  addMymail(){
        this.myMail=true;
      this.myName =firebase.auth().currentUser.email;
      this.ifClicked=true;
      this.ifClicked1=false;
  }

  deleteUsers(key,u){
    console.log(u);
   this.af.database.object('/group/' + key).remove();
  }

  checkArr(){

  }

  constructor(private _groupsService: ChatRoomsService,public af:AngularFire,public afService: AF) { }

  ngOnInit() {
    this._groupsService.getUsers()
    .subscribe(users => {this.users = users});
    this._groupsService.getGroups()
    .subscribe(groups => {this.groups = groups});
    console.log(this.groups);
  }

}
