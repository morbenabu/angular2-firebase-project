import { Component, OnInit, Output, EventEmitter  } from '@angular/core';
import {AF} from "./../providers/af"
import {HomePageService} from '../home-page/home-page.service';
 import {NgForm} from '@angular/forms';
 import { FirebaseListObservable, AngularFire } from "angularfire2";
import * as firebase from 'firebase';

@Component({
  selector: 'app-home-page-son',
  templateUrl: './home-page-son.component.html',
  styleUrls: ['./home-page-son.component.css'],
  inputs:['user']
})
export class HomePageSonComponent implements OnInit {
  @Output() editEvent = new EventEmitter<HomePageSonComponent>();
  @Output() deleteEvent = new EventEmitter<HomePageSonComponent>();
  @Output() selectEvent = new EventEmitter<HomePageSonComponent>();

user:HomePageSonComponent;
isSelectUser=true;

  sendcancel(){
  this.deleteEvent.emit(this.user);
  this.isSelectUser=true;
}
  sendselect(){
  this.selectEvent.emit(this.user);
  this.isSelectUser=false;
}

  constructor() { }

  ngOnInit() {
  }

}
