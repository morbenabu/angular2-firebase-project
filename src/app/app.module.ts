import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AngularFireModule } from 'angularfire2';
import { LoginPageComponent } from './login-page/login-page.component';
import {RouterModule, Routes} from "@angular/router";
import {AF} from "./providers/af";
import { HomePageComponent } from './home-page/home-page.component';
import {FormsModule} from "@angular/forms";
import { RegistrationPageComponent } from './registration-page/registration-page.component';
import { ChatComponent } from './chat/chat.component';
import { ProfileComponent } from './profile/profile.component';
//import { DatePickerModule } from 'ng2-datepicker';
import { ChatService } from './chat/chat.service';
import { HomePageService } from './home-page/home-page.service';
import { HomePageSonComponent } from './home-page-son/home-page-son.component';
import { ChatRoomsComponent } from './chat-rooms/chat-rooms.component';
import { ChatRoomsService } from './chat-rooms/chat-rooms.service';






export const firebaseConfig = {
   apiKey: "AIzaSyAdIuhYXK1QgVbNEqYSvh-ZGrNp0_I2Zak",
    authDomain: "chatapp-ad844.firebaseapp.com",
    databaseURL: "https://chatapp-ad844.firebaseio.com",
    projectId: "chatapp-ad844",
    storageBucket: "chatapp-ad844.appspot.com",
    messagingSenderId: "1066455720351"
};

const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'login', component: LoginPageComponent },
  { path: 'register', component: RegistrationPageComponent},
  { path: 'chat', component: ChatComponent},
  { path: 'profile' , component: ProfileComponent},
  { path: 'chatRooms' , component: ChatRoomsComponent},
  
];

@NgModule({
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(firebaseConfig),
    RouterModule.forRoot(routes),
    FormsModule
   // DatePickerModule 
  ],

  declarations: [ AppComponent, LoginPageComponent, HomePageComponent, RegistrationPageComponent, ChatComponent, ProfileComponent, HomePageSonComponent, ChatRoomsComponent],

  bootstrap: [ AppComponent ],
  providers: [AF,ChatService, HomePageService,ChatRoomsService]
})

export class AppModule { }