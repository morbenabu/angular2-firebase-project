import { Component, OnInit ,Input} from '@angular/core';
import {AF} from "./../providers/af";
import {Router} from "@angular/router";
import { FirebaseListObservable, AngularFire } from "angularfire2";
import * as firebase from 'firebase';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
    
    public userinfo :userInfo;

    targetRef:any;
    storageRef:any;

    public user = firebase.auth().currentUser;
    public url;
    email = this.user.email;
    UserName = this.user.displayName;
    photo = this.user.photoURL;


    public users : FirebaseListObservable<any>;

  
  constructor(public afService: AF, private router: Router) {
    this.storageRef = firebase.storage().ref();
    this.users = this.afService.users;


  }
 
  updateProfile(){
      console.log(this.userinfo.city);
      if(this.userinfo.city!= "")
        firebase.database().ref('registeredUsers/'+ this.user.uid).update({city : this.userinfo.city});
        console.log("dsfsdfsdf       "+this.user.uid);
      if(this.userinfo.dob != "")
        firebase.database().ref('registeredUsers/'+ this.user.uid).update({dob : this.userinfo.dob});
  }

  upload(event:any){
         let targetFile = event.srcElement.files[0];
         let fbsPath = 'images/' + targetFile.name;
         console.log("The Path:" +fbsPath);
          this.uploadFile(fbsPath,targetFile);
                  console.log("dsfsdfsdf       "+this.user.uid);

  }

  uploadFile(fbsPath,targetFile) {
      let promise = new Promise((res,rej) => {
        this.targetRef =this.storageRef.child(fbsPath);
        let task=this.targetRef.put(targetFile);
        task.on('state_changed',
          (snapshot:any) => {
            console.log(snapshot.state);
          },
          (error:any) => {
            console.log(error.code);
            rej(error);
          },
          () => {
            let downloadUrl = task.snapshot.downloadURL;
            this.url = downloadUrl;
            console.log(downloadUrl);
            res(downloadUrl);
            this.user.updateProfile({displayName : this.user.displayName,photoURL: downloadUrl}).then(function(){console.log("updated!!");},function(error){});
            alert("Photo Changed!");
          }
        );
      })      
      return promise;
    }

  ngOnInit() {
    
        
  
    
    
    this.userinfo = {city : "", dob : "" ,name : ""};
    
  }
}

export class userInfo{
    name : String;
    city : String;
    dob : String;
}