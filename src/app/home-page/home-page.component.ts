import {Component, OnInit } from '@angular/core';
import {AF} from "./../providers/af"
import {HomePageService} from './home-page.service';
 import {NgForm} from '@angular/forms';
 import { FirebaseListObservable, AngularFire } from "angularfire2";
import * as firebase from 'firebase';


import {Injectable} from "@angular/core";

import {FirebaseObjectFactoryOpts} from "angularfire2/interfaces";

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css'
  ]
})
export class HomePageComponent implements OnInit {

  currentUser;
  users;
  check;
  check1;
  user;
  homePageObservable;
  saveStatus;

 getUsers() {
  return this.users;
}

updateUser(status,user){
if(status==1)
  {
   firebase.database().ref('registeredUsers/'+ user).update({status : "2"});
    firebase.database().ref('group/'+ user).update({name : "2"});
   this.saveStatus=2;
  }
   else
   {
   firebase.database().ref('registeredUsers/'+ user).update({status : "1"});
   this.saveStatus=1;
   
   }

}

 clicked(){
   this.check=true;
 }

addGroup(name){
    this._homPageServise.addGroup(name);
  }

  constructor(public afService:AF,private _homPageServise: HomePageService,private af:AngularFire) { 
    console.log("in home page"+this.afService.status);
  }



  
  ngOnInit() {
    this._homPageServise.getUsers()
    .subscribe(users => this.users = users);
  }
}

export class group{
  name:string;
}