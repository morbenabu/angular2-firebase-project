import { Injectable } from '@angular/core';
import {AngularFire,FirebaseListObservable, FirebaseObjectObservable} from 'angularfire2';

@Injectable()
export class HomePageService {

  homePageObservable;
  currentUser;
  firebase;
  public user: FirebaseObjectObservable<any>;
  public messages: FirebaseListObservable<any>;
  public users: FirebaseListObservable<any>;


getUsers(){
   this.homePageObservable = this.af.database.list('/registeredUsers');
   return this.homePageObservable;    
  }

  addGroup(name){
    this.homePageObservable.push(name);
  }

updateUser(user){
    let userKey = user.$key;
    let userData = {status : user.status};
    this.af.database.object('/registeredUsers/' + userKey).update(userData);
  }



  constructor(private af:AngularFire) {}   

}
