import {Injectable} from "@angular/core";
import {AngularFire, AuthProviders, AuthMethods, FirebaseListObservable, FirebaseObjectObservable} from 'angularfire2';
import {FirebaseObjectFactoryOpts} from "angularfire2/interfaces";
import * as firebase from 'firebase';

@Injectable()
export class AF {
  public messages: FirebaseListObservable<any>;
  public registeredUsers: FirebaseListObservable<any>;
  public users: FirebaseListObservable<any>;
  public displayName: string;
  public email: string;
  public nameOfGroup="~general Chet~";
  public name: string;
  public status: string;
  public user: FirebaseObjectObservable<any>;
  public event: FirebaseListObservable<any>;
  public group: FirebaseListObservable<any>;
  public storageRef : any;
  public targetRef : any;
   public groups: FirebaseListObservable<any>;

  


  constructor(public af: AngularFire) {
    this.af.auth.subscribe(
      (auth) => {
        if (auth != null) {
          this.user = this.af.database.object('users/' + auth.uid);
        }
      });

    this.targetRef = firebase.storage().ref();
    this.messages = this.af.database.list("messages");
    this.users = this.af.database.list("users");
    this.group = this.af.database.list("group");
    this.status = "1";
  }

addEvent(item){
  this.event.push({
      location: item.location,
      time: item.time,
      note: item.note
    });
}

  /**
   * Logs in the user
   * @returns {firebase.Promise<FirebaseAuthState>}
   */
  loginWithGoogle() {
    return this.af.auth.login({
      provider: AuthProviders.Google,
      method: AuthMethods.Popup,
    });
  }


emailVerfication()
{
  return this.af.auth.getAuth().auth.sendEmailVerification();
}

  /**
   * Logs out the current user
   */
  logout() {
    return this.af.auth.logout();
  }

  chooseUsers(){
  firebase.auth().currentUser.uid;
}

  addUserInfo(){
    return this.af.database.object('registeredUsers/' + firebase.auth().currentUser.uid).set({
      name: this.displayName,
      email: this.email,
      status:this.status,
      city : "",
      dob : ""
    });
  }

  /**
   * Saves a message to the Firebase Realtime Database
   * @param text
   */
  sendMessage(text,nameofgroup) {
    var message = {
      message: text,
      displayName: this.displayName,
      email: this.email,
      timestamp: Date.now(),
      nameOfGroup: nameofgroup,
    };
    this.messages.push(message);
  }

  addGroup(text,arr){
    var details = {
      name: text,
      displayName: this.displayName,
      email: this.email,
      userArr:arr,
      admin:arr[0]

    };
    this.group.push(details);
  }



  /**
   *
   * @param model
   * @returns {firebase.Promise<void>}
   */
  registerUser(email, password){
    console.log(email);
    console.log(status);
    return this.af.auth.createUser({
      email: email,
      password: password
    });


  }

  /**
   *
   * @param uid
   * @param model
   * @returns {firebase.Promise<void>}
   */
  saveUserInfoFromForm(uid, name, email,status) {
    console.log("from the userinfo"+status);
    return this.af.database.object('registeredUsers/' + uid).set({
      name: name,
      email: email,
      status:status,
      city : "",
      dob : ""
    });
    
  }

  /**
   * Logs the user in using their Email/Password combo
   * @param email
   * @param password
   * @returns {firebase.Promise<FirebaseAuthState>}
   */
  loginWithEmail(email, password){//,status) {
    return this.af.auth.login({
        email: email,
        password: password
      },
      {
        provider: AuthProviders.Password,
        method: AuthMethods.Password,
      });
  }


}